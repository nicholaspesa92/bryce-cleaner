import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class AuthService {

  public user: any;

  constructor(
    private a: AngularFireAuth,
    private afs: AngularFirestore
  ) {
    this.user = this.a.authState;
  }

  createEmailAuth(email, password) {
    return this.a.auth.createUserWithEmailAndPassword(email, password);
  }

  getFirebaseUser(uid) {
    return this.afs.doc(`users/${uid}`)
      .snapshotChanges()
      .map((user) => {
        const id = user.payload.id;
        const data  = user.payload.data();
        return { id, ...data };
      });
  }

  updateFirebaseUser(uid, data) {
    return this.afs.doc(`users/${uid}`).update(data);
  }

  getToken() {
    return this.a.auth.currentUser.getIdToken();
  }

}
