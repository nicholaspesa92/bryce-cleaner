"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
function authenticateUser(req, res, next) {
    let authToken;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        authToken = req.headers.authorization.split('Bearer ')[1];
        console.log(authToken);
    }
    else {
        res.status(403).send({ text: 'Must provide a header that looks like "Authorization: Bearer <Firebase ID Token>"' });
    }
    config_1.auth.verifyIdToken(authToken)
        .then(decodedToken => {
        console.log(decodedToken);
        req.user = decodedToken;
        next();
    }).catch((error) => {
        res.status(403).send(error);
    });
}
exports.authenticateUser = authenticateUser;
function getUser(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield config_1.db.collection(`users`).doc(userId).get().then((doc) => { return doc.data(); });
    });
}
exports.getUser = getUser;
function createCustomer(fUser) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield config_1.stripe.customers.create({
            email: fUser.email,
            metadata: { firebaseUID: fUser.uid }
        });
    });
}
exports.createCustomer = createCustomer;
function getCustomer(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield getUser(userId);
        const cusId = user.stripeCustomerId;
        return yield config_1.stripe.customers.retrieve(cusId);
    });
}
exports.getCustomer = getCustomer;
function updateCustomer(userId, cusData) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield getUser(userId);
        const cusId = user.stripeCustomerId;
        return yield config_1.stripe.customers.update(cusId, cusData);
    });
}
exports.updateCustomer = updateCustomer;
function attachSource(userId, sourceId) {
    return __awaiter(this, void 0, void 0, function* () {
        const customer = yield getCustomer(userId);
        console.log(customer);
        const existingSource = customer.sources.data.filter(source => source.id === sourceId).pop();
        return existingSource ? existingSource : yield config_1.stripe.customers.createSource(customer.id, { source: sourceId });
    });
}
exports.attachSource = attachSource;
function createCharge(userId, sourceId, amount, currency) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield getUser(userId);
        const cusId = user.stripeCustomerId;
        const card = yield attachSource(userId, sourceId);
        return yield config_1.stripe.charges.create({
            amount: amount,
            currency: currency || 'usd',
            customer: cusId,
            source: sourceId
        });
    });
}
exports.createCharge = createCharge;
function getUserCharges(userId, limit) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield getUser(userId);
        const cusId = user.stripeCustomerId;
        return yield config_1.stripe.charges.list({
            limit,
            customer: cusId
        });
    });
}
exports.getUserCharges = getUserCharges;
// Subscriptions //
function createSubscription(userId, sourceId, planData, metadata) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield getUser(userId);
        const cusId = user.stripeCustomerId;
        const card = yield attachSource(userId, sourceId);
        const plan = yield createPlan(userId, planData);
        console.log('Helper Method: ', plan);
        const sub = yield config_1.stripe.subscriptions.create({
            customer: cusId,
            items: [
                { plan: plan.id }
            ],
            metadata: metadata
        });
        const subs = {
            [plan.id]: 'active'
        };
        yield config_1.db.doc(`users/${userId}`).set({ subs }, { merge: true });
        return sub;
    });
}
exports.createSubscription = createSubscription;
function getPlans(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield getUser(userId);
        const cusId = user.stripeCustomerId;
        const sub = yield config_1.stripe.plans.list();
        return sub;
    });
}
exports.getPlans = getPlans;
function createPlan(userId, planData) {
    return __awaiter(this, void 0, void 0, function* () {
        const plan = yield config_1.stripe.plans.create({
            amount: planData.price,
            interval: planData.interval,
            interval_count: planData.int_count,
            currency: 'usd',
            product: {
                name: `Product for UID: ${userId}`
            },
            id: userId
        });
        return plan;
    });
}
exports.createPlan = createPlan;
// TO DO //
function sendConfirmationEmail(userId, data) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield getUser(userId);
        let from, to, subject, body;
        const dummy = 'npesa1992@gmail.com';
        const id = data.id.substring(0, 6);
        body = `
        <table style="border: none">
            <tr><h2>Confirmation #: ${id}</h2></tr>
            <tr><p>Congratulations on signing up for Descutes Home Cleaning.</p></tr>
            <tr><p>We will be reaching out to you shortly</p></tr>
        </table>
    `;
        // SEND CUSTOMER EMAIL //
        from = new config_1.sgmail.Email('kyle@deschuteshomecleaning.com');
        to = new config_1.sgmail.Email(data.email);
        subject = 'Deschutes Home Cleaning';
        const content = new config_1.sgmail.Content('text/html', body);
        const cusmail = new config_1.sgmail.Mail(from, subject, to, content);
        const cusreq = config_1.sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: cusmail.toJSON()
        });
        config_1.sg.API(cusreq, (error, res) => {
            if (error) {
                console.log(error);
                return error;
            }
            console.log(res);
            return res;
        });
        // END SENDING CUSTOMER EMAIL //
        //*****************************//
        // SEND ADMIN EMAIL //
        // END SENDING ADMIN EMAIL //
    });
}
exports.sendConfirmationEmail = sendConfirmationEmail;
//# sourceMappingURL=helpers.js.map