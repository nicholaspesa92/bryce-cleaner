require('./app/config');
import * as auth from './app/auth';
import { api } from './app/api';

export const app = api;

export const createStripeCustomer = auth.createStripeCustomer;

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

