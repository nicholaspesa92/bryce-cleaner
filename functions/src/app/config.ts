import * as CORS from 'cors';
export const cors = CORS({ origin: true });

import * as express from 'express';
export const app = express();

import { authenticateUser } from './helpers';

app.use(cors);
app.use(authenticateUser);

import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp(functions.config().firebase);

export const db = admin.firestore();
export const auth = admin.auth();

import * as Stripe from 'stripe';

export const stripeTestSecretKey = functions.config().stripe.testsecret;
export const stripeLiveSecretKey = functions.config().stripe.livesecret;

export const stripe = new Stripe(stripeLiveSecretKey);

import * as helper from 'sendgrid';

export const sgmail = helper.mail;
export const sg = helper(functions.config().sendgrid.key);