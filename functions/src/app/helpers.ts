import { stripe, db, auth, sgmail, sg } from './config';

export function authenticateUser(req, res, next): void {
    let authToken;

    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        authToken = req.headers.authorization.split('Bearer ')[1];
        console.log(authToken);
    } else {
        res.status(403).send({ text: 'Must provide a header that looks like "Authorization: Bearer <Firebase ID Token>"' });
    }

    auth.verifyIdToken(authToken)
        .then(decodedToken => {
            console.log(decodedToken);
            req.user = decodedToken;
            next();
        }).catch((error) => {
            res.status(403).send(error);
        });
}

export async function getUser(userId: string): Promise<any> {
    return await db.collection(`users`).doc(userId).get().then((doc) => { return doc.data(); });
}

export async function createCustomer(fUser: any): Promise<any> {
    return await stripe.customers.create({
        email: fUser.email,
        metadata: { firebaseUID: fUser.uid }
    });
}

export async function getCustomer(userId: string): Promise<any> {
    const user = await getUser(userId);
    const cusId = user.stripeCustomerId;

    return await stripe.customers.retrieve(cusId);
}

export async function updateCustomer(userId: string, cusData: any): Promise<any> {
    const user = await getUser(userId);
    const cusId = user.stripeCustomerId;

    return await stripe.customers.update(cusId, cusData);
}

export async function attachSource(userId: string, sourceId: string): Promise<any> {
    const customer = await getCustomer(userId);
    console.log(customer);
    const existingSource = customer.sources.data.filter(source => source.id === sourceId).pop();

    return existingSource ? existingSource : await stripe.customers.createSource(customer.id, { source: sourceId });
}

export async function createCharge(userId: string, sourceId: string, amount: number, currency?: string): Promise<any> {
    const user = await getUser(userId);
    const cusId = user.stripeCustomerId;
    const card = await attachSource(userId, sourceId);

    return await stripe.charges.create({
        amount: amount,
        currency: currency || 'usd',
        customer: cusId,
        source: sourceId
    });
}

export async function getUserCharges(userId: string, limit?: number): Promise<any> {
    const user = await getUser(userId);
    const cusId = user.stripeCustomerId;

    return await stripe.charges.list({
        limit,
        customer: cusId
    });
}

// Subscriptions //
export async function createSubscription(userId: string, sourceId: string, planData, metadata): Promise<any> {
    const user = await getUser(userId);
    const cusId = user.stripeCustomerId;
    const card = await attachSource(userId, sourceId);
    const plan = await createPlan(userId, planData);
    console.log('Helper Method: ', plan);

    const sub = await stripe.subscriptions.create({
        customer: cusId,
        items: [
            { plan: plan.id }
        ],
        metadata: metadata
    });

    const subs = {
        [plan.id]: 'active'
    };

    await db.doc(`users/${userId}`).set({ subs }, { merge: true });

    return sub;
}

export async function getPlans(userId) {
    const user = await getUser(userId);
    const cusId = user.stripeCustomerId;

    const sub = await stripe.plans.list();
    return sub;
}

export async function createPlan(userId, planData) {
    const plan = await stripe.plans.create({
        amount: planData.price,
        interval: planData.interval,
        interval_count: planData.int_count,
        currency: 'usd',
        product: {
            name: `Product for UID: ${userId}`
        },
        id: userId
    });
    return plan;
}

// TO DO //
export async function sendConfirmationEmail(userId: string, data: any): Promise<any> {
    const user = await getUser(userId);
    let from, to, subject, body;

    const dummy = 'npesa1992@gmail.com';

    const id = data.id.substring(0,6);

    body = `
        <table style="border: none">
            <tr><h2>Confirmation #: ${id}</h2></tr>
            <tr><p>Congratulations on signing up for Descutes Home Cleaning.</p></tr>
            <tr><p>We will be reaching out to you shortly</p></tr>
        </table>
    `
    // SEND CUSTOMER EMAIL //
    from = new sgmail.Email('kyle@deschuteshomecleaning.com');
    to = new sgmail.Email(data.email);
    subject = 'Deschutes Home Cleaning';

    const content = new sgmail.Content('text/html', body);
    const cusmail = new sgmail.Mail(from, subject, to, content);

    const cusreq = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: cusmail.toJSON()
    });

    sg.API(cusreq, (error, res) => {
        if (error) { console.log(error); return error; }
        console.log(res);
        return res;
    });
    // END SENDING CUSTOMER EMAIL //

    //*****************************//

    // SEND ADMIN EMAIL //

    // END SENDING ADMIN EMAIL //
}
