export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyA94tGGmOFv6Mfy3TC4hg3CeghZCeFKIEE',
    authDomain: 'bryce-sandbox.firebaseapp.com',
    databaseURL: 'https://bryce-sandbox.firebaseio.com',
    projectId: 'bryce-sandbox',
    storageBucket: '',
    messagingSenderId: '909298163013'
  },
  functionsUrl: 'https://us-central1-bryce-sandbox.cloudfunctions.net/app',
  stripePub: 'pk_live_XpuMvsdf7Ovnw2Nt1FtNxUK8'
};
