"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('./app/config');
const auth = require("./app/auth");
const api_1 = require("./app/api");
exports.app = api_1.api;
exports.createStripeCustomer = auth.createStripeCustomer;
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
//# sourceMappingURL=index.js.map