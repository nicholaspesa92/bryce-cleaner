import { Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { NgForm } from '@angular/forms';
import { switchMap, map } from 'rxjs/operators';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { HttpClient } from '@angular/common/http';

import { PaymentService } from './../ng-services/payment.service';
import { AuthService } from './../ng-services/auth.service';
import { environment } from './../../environments/environment';

@Component({
  selector: 'app-design-one',
  templateUrl: './design-one.component.html',
  styleUrls: ['./design-one.component.scss']
})
export class DesignOneComponent implements OnInit, AfterViewInit, OnDestroy {

  stepGroup: FormGroup;
  step: number;
  directions: any[];

  stripe = Stripe(environment.stripePub);
  elements: any;

  roomAmount: number;
  frequencies: any;
  frequency: any;

  instructions: any;

  user$: Observable<any>;
  user: any;
  email: string;

  plans: string[];

  cardToken: any;

  @ViewChild('cardInfo') cardInfo: ElementRef;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  functionsUrl: string;

  total: number;
  deepValues: any;

  days: any[];
  sDay: any;

  loading: boolean;
  startingSub: boolean;
  duplicateUser: boolean;
  weakPW: boolean;

  activeSubscription: any;

  constructor(
    private pms: PaymentService,
    private fb: FormBuilder,
    private as: AuthService,
    private cd: ChangeDetectorRef,
    private http: HttpClient
  ) {
    this.step = 0;
    this.roomAmount = 1;
    this.total = 0;
    // tslint:disable-next-line:max-line-length
    this.directions = ['Choose your setup', 'Deep Clean Options', 'Enter your information', 'Create a password', 'Finalize payment', 'Confirmation'];
    this.days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
    this.sDay = -1;
    this.email = '';
    this.functionsUrl = environment.functionsUrl;
    this.elements = this.stripe.elements();
    this.loading = false;
    this.startingSub = false;
    this.duplicateUser = false;
    this.weakPW = false;
  }

  ngOnInit() {
    this.buildForm();
    this.setDeepValues();
    this.setFrequencies();
    this.setInstructions();
    this.calcTotal();
  }

  ngAfterViewInit() {
    const style = {
      base: {
        lineHeight: '24px',
        fontFamily: 'monospace',
        fontSmoothing: 'antialiased',
        fontSize: '19px',
        '::placeholder': {
          color: 'purple'
        }
      }
    };


    this.card = this.elements.create('card', { style });
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form?: NgForm) {
    this.startingSub = true;
    const { source, errors } = await this.stripe.createSource(this.card);

    if (errors) {
      console.log('Something is wrong:', errors);
    } else {
      const s1 = this.stepGroup.controls['stepOne'].value;
      console.log(s1);
      let interval;
      let int_count;
      if (s1.frequency === 'weekly' || s1.frequency === 'biWeekly') {
        interval = 'week';
        s1.frequency === 'biWeekly' ? int_count = 2 : int_count = 1;
      } else if (s1.fequency === 'once') {
        interval = 'month';
        int_count = 1;
      } else {
        interval = 'month',
        int_count = 1;
      }
      this.frequency = s1.frequency;
      if (s1.frequency === 'once') {
        console.log('one-time');
        const body = {
          sourceId: source.id,
          amount: this.total * 100,
          currency: 'usd'
        };
        const t = await this.as.getToken();
        const req = this.pms.createCharge(body, t);
        req.subscribe((res) => {
          console.log(res);
          this.updateStripeCustomer();
        });
      } else {
        const body = {
          sourceId: source.id,
          planData: {
            price: this.total * 100,
            interval: interval,
            int_count: int_count
          },
          metadata: {
            day: this.days[this.sDay]
          }
        };
        const t = await this.as.getToken();
        const req = this.pms.createSubscription(body, t);
        req.subscribe((res) => {
          console.log(res);
          this.activeSubscription = res;
          this.updateStripeCustomer();
        });
      }
    }
  }

  async updateStripeCustomer() {
    const s2 = this.stepGroup.controls['stepTwo'].value;
    const body = {
      cusData: {
        shipping: {
          address: {
            line1: s2.street,
            city: s2.city,
            state: s2.state,
            postal_code: s2.zip
          },
          name: s2.name,
          phone: s2.phoneNumber
        },
        metadata: {
          day: this.days[this.sDay]
        }
      }
    };
    const t = await this.as.getToken();
    const r = await this.pms.updateCustomer(body, t).toPromise();
    this.saveUserAfterSub(this.user.id);
  }

  async saveUserAfterSub(uid) {
    const x = this.stepGroup;
    const s1 = x.controls['stepOne'].value;
    const s2 = x.controls['stepTwo'].value;
    const data = {
      name: s2.name,
      address: {
        street: s2.street,
        city: s2.city,
        state: s2.state,
        zip: s2.zip
      },
      email: s2.email,
      phoneNumber: s2.phoneNumber,
      company: s2.company,
      rooms: this.roomAmount,
      frequency: s1.frequency,
      day: this.days[this.sDay]
    };
    const u = await this.as.updateFirebaseUser(uid, data);
    this.startingSub = false;
    this.step += 1;
    this.sendEmail();
  }

  async sendEmail() {
    const t = await this.as.getToken();
    const data = this.user;
    const body = { data };
    console.log(body);
    const req = await this.pms.sendEmails(body, t).toPromise();
    console.log(req);
  }

  async createUser() {
    this.loading = true;
    const x = this.stepGroup.controls['stepFour'].value;
    try {
      const user = await this.as.createEmailAuth(x.email, x.password);
      this.startUserSub(user.uid);
    } catch (err) {
      this.loading = false;
      console.log(err);
      if (err.code === 'auth/email-already-in-use') {
        this.duplicateUser = true;
      } else if (err.code === 'auth/weak-password') {
        this.weakPW = true;
      }
    }
  }

  startUserSub(uid) {
    this.user$ = this.as.getFirebaseUser(uid);
    this.user$.subscribe((fuser) => {
      if (fuser.stripeCustomerId) {
        console.log(fuser);
        this.user = fuser;
        this.loading = false;
        this.step === 3 ? this.step += 1 : console.log('second-time-receiving-user?');
      }
    });
  }

  async addStripeSource() {
    const t = await this.as.getToken();
    console.log(t);
    const { s, e } = await this.stripe.createSource(this.card);
    if (e) {
      console.log('Error: ', e);
    } else {
      console.log(s);
      const req = this.pms.attachSource(s, t);
      req.subscribe((res) => {
        console.log(res);
        this.saveUserAfterSub(this.user.uid);
      });
    }
  }

  async getPlans() {
    const t = await this.as.getToken();
    const req = this.pms.getPlans(t);
    req.subscribe((res) => {
      console.log(res);
    });
  }

  calcTotal() {
    const x = this.stepGroup;
    const s1 = x.controls['stepOne'].value;
    const sD = x.controls['stepDeep'].value;
    const dV = this.deepValues;

    const rooms = this.roomAmount;
    const frequency = this.frequencies[s1.frequency];

    const interiorWindow = sD.interiorWindow * dV.interiorWindow;
    const exteriorWindow = sD.exteriorWindow * dV.exteriorWindow;
    const windowTracks = sD.windowTracks * dV.windowTracks;
    const insideFridge = sD.insideFridge ? dV.insideFridge : 0;
    const dishes = sD.dishes ? dV.dishes : 0;
    const ceilingFans = sD.ceilingFans * dV.ceilingFans;
    const ovenCleaning = sD.ovenCleaning ? dV.ovenCleaning : 0;
    const cabinets = sD.cabinets ? dV.cabinets : 0;
    const handFloors = sD.handFloors ? dV.handFloors : 0;
    const handBlinds = sD.handBlinds * dV.handBlinds;
    const handShower = sD.handShower ? dV.handShower : 0;
    const hardShower = sD.hardShower ? dV.hardShower : 0;
    const handWalls = sD.handWalls ? dV.handWalls : 0;
    const eco = sD.eco ? dV.eco : 0;

    const t = (rooms * frequency) +
      interiorWindow +
      exteriorWindow +
      windowTracks +
      insideFridge +
      dishes +
      ceilingFans +
      ovenCleaning +
      cabinets +
      handFloors +
      handBlinds +
      handShower +
      hardShower +
      handWalls +
      eco;

      this.total = t ? t : 0;
      console.log(this.total);
  }

  buildForm() {
    this.stepGroup = this.fb.group({
      stepOne: this.fb.group({
        rooms: [this.roomAmount, Validators.required],
        frequency: ['weekly', Validators.required]
      }),
      stepDeep: this.fb.group({
        interiorWindow: [0, Validators.required],
        exteriorWindow: [0, Validators.required],
        windowTracks: [0, Validators.required],
        insideFridge: [false, Validators.required],
        dishes: [false, Validators.required],
        ceilingFans: [0, Validators.required],
        ovenCleaning: [false, Validators.required],
        cabinets: [false, Validators.required],
        handFloors: [false, Validators.required],
        handBlinds: [0, Validators.required],
        handShower: [false, Validators.required],
        hardShower: [false, Validators.required],
        handWalls: [false, Validators.required],
        eco: [false, Validators.required]
      }),
      stepTwo: this.fb.group({
        name: ['', Validators.required],
        email: ['', [Validators.email, Validators.required]],
        phoneNumber: ['', [Validators.required]],
        company: ['', Validators.required],
        street: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        zip: ['', Validators.required]
      }),
      stepThree: this.fb.group({
        cardNumber: ['', Validators.required],
        expM: ['', Validators.required],
        expY: ['', Validators.required],
        cvc: ['', Validators.required]
      }),
      stepFour: this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required]
      })
    });
    this.stepOneValid();
  }

  setDeepValues() {
    this.deepValues = {
      interiorWindow: 2,
      exteriorWindow: 3,
      windowTracks: 2,
      insideFridge: 15,
      dishes: 5,
      ceilingFans: 10,
      ovenCleaning: 30,
      cabinets: 15,
      handFloors: 35,
      handBlinds: 6,
      handShower: 25,
      hardShower: 75,
      handWalls: 15,
      eco: 4
    };
  }

  setFrequencies() {
    this.frequencies = {
      weekly: 17,
      biWeekly: 19,
      monthly: 23,
      once: 23
    };
  }

  stepOneValid() {
    console.log(this.stepGroup.controls['stepOne'].invalid);
    return this.stepGroup.controls['stepOne'].invalid;
  }

  checkStep() {
    let v = false;
    switch (this.step) {
      case 0:
        v = this.stepGroup.controls['stepOne'].invalid;
        break;
      case 1:
        v = this.stepGroup.controls['stepTwo'].invalid;
        break;
      case 2:
        this.error ? v = true : v = false;
        break;
      case 3:
        v = false;
        break;
    }
    return v;
  }

  increment() {
    switch (this.step) {
      case 0:
        this.step += 1;
        break;
      case 1:
        this.step += 1;
        break;
      case 2:
        this.stepGroup.controls['stepFour'].patchValue({
          email: this.stepGroup.controls['stepTwo'].value.email
        });
        this.step += 1;
        break;
      case 3:
        // this.onSubmit();
        this.createUser();
        break;
      case 4:
        this.onSubmit();
        break;
    }
  }

  setInstructions() {
    this.instructions = [
      { text: 'Test instructions for the margin to make sure we straddle the middle of the circle above and not below.' },
      { text: 'Test instructions for the margin to make sure we straddle the middle of the circle above and not below.' },
      { text: 'Test instructions for the margin to make sure we straddle the middle of the circle above and not below.' }
    ];
  }
}
