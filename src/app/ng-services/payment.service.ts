import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { switchMap, map } from 'rxjs/operators';
import { fromPromise } from 'rxjs/observable/fromPromise';

import { environment } from './../../environments/environment';

@Injectable()
export class PaymentService {

  functionsUrl: string;

  private stripe = Stripe(environment.stripePub);
  elements: any;

  constructor(
    private http: HttpClient
  ) {
    this.elements = this.stripe.elements();
    this.functionsUrl = environment.functionsUrl;
  }

  createCharge(body, token) {
    return this.http.post(`${this.functionsUrl}/charges`, body, { headers: this.getHeaders(token) });
  }

  createSubscription(body, token) {
    return this.http.post(`${this.functionsUrl}/subscriptions`, body, { headers: this.getHeaders(token) });
  }

  updateCustomer(body, token) {
    return this.http.post(`${this.functionsUrl}/customer`, body, { headers: this.getHeaders(token) });
  }

  attachSourceX(body, token) {
    return this.http.post(`${this.functionsUrl}/sources/`, body, { headers: this.getHeaders(token) });
  }

  attachSource(sourceId, token) {
    const url = `${this.functionsUrl}/sources/`;
    return this.http.post(url, { sourceId: sourceId }, { headers: this.getHeaders(token) });
  }

  getPlans(token) {
    const url = `${this.functionsUrl}/plans/`;
    return this.http.get(url, { headers: this.getHeaders(token) });
  }

  sendEmails(body, token) {
    const url = `${this.functionsUrl}/email`;
    return this.http.post(url, body, { headers: this.getHeaders(token) });
  }

  getHeaders(token) {
    return new HttpHeaders().set('Authorization', `Bearer ${token}`);
  }

}
