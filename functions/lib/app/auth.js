"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const config_1 = require("./config");
const helpers_1 = require("./helpers");
exports.createStripeCustomer = functions.auth.user().onCreate((event) => {
    const user = event.data;
    const uRef = config_1.db.collection(`users`).doc(user.uid);
    return helpers_1.createCustomer(user)
        .then((customer) => {
        const data = { stripeCustomerId: customer.id };
        return uRef.set(data, { merge: true });
    }).catch((error) => {
        console.log(error);
    });
});
//# sourceMappingURL=auth.js.map