// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA94tGGmOFv6Mfy3TC4hg3CeghZCeFKIEE',
    authDomain: 'bryce-sandbox.firebaseapp.com',
    databaseURL: 'https://bryce-sandbox.firebaseio.com',
    projectId: 'bryce-sandbox',
    storageBucket: '',
    messagingSenderId: '909298163013'
  },
  functionsUrl: 'https://us-central1-bryce-sandbox.cloudfunctions.net/app',
  stripePub: 'pk_test_wBJKVcp9XneKsOkcNh6revTI'
};
