import * as functions from 'firebase-functions';
import { db } from './config';
import { createCustomer } from './helpers';

export const createStripeCustomer = functions.auth.user().onCreate((event) => {
    const user = event.data;
    const uRef = db.collection(`users`).doc(user.uid);

    return createCustomer(user)
        .then((customer) => {
            const data = { stripeCustomerId: customer.id };
            return uRef.set(data, { merge: true });
        }).catch((error) => {
            console.log(error);
        });
})