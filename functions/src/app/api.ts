import * as functions from 'firebase-functions';
import * as helpers from './helpers';
import { app } from './config';

// CHARGES //
app.post('/charges', (req: any, res) => {
    const userId = req.user.uid;
    const sourceId = req.body.sourceId;
    const amount = req.body.amount;
    const currency = req.body.currency;

    const promise = helpers.createCharge(userId, sourceId, amount, currency);
    defaultHandler(promise, res);
});
app.get('/charges', (req: any, res) => {
    const userId = req.user.uid;

    const promise = helpers.getUserCharges(userId);
    defaultHandler(promise, res);
});

// POST Sources //
app.post('/sources', (req: any, res) => {
    console.log(req.user);
    const userId = req.user.uid;
    const sourceId = req.body.sourceId;

    const promise = helpers.attachSource(userId, sourceId);
    defaultHandler(promise, res);
});

// GET Customer //
app.get('/customer', (req: any, res) => {
    const userId = req.user.uid;

    const promise = helpers.getCustomer(userId);
    defaultHandler(promise, res);
});

app.post('/customer', (req: any, res) => {
    const userId = req.user.uid;
    const cusData = req.body.cusData;

    const promise = helpers.updateCustomer(userId, cusData);
    defaultHandler(promise, res);
});

// POST Subscriptions //
app.post('/subscriptions', (req: any, res) => {
    const userId = req.user.uid;
    const sourceId = req.body.sourceId;
    const planData = req.body.planData;
    const metadata = req.body.metadata;

    const promise = helpers.createSubscription(userId, sourceId, planData, metadata);
    defaultHandler(promise, res);
});

app.get('/plans', (req: any, res) => {
    const userId = req.user.uid;

    const promise = helpers.getPlans(userId);
    defaultHandler(promise, res);
});

app.post('/email', (req: any, res) => {
    const userId = req.user.uid;
    const data = req.body.data;

    const promise = helpers.sendConfirmationEmail(userId, data);
    defaultHandler(promise, res);
})

function defaultHandler(promise: Promise<any>, res: any): void {
    promise
        .then(data => res.status(200).send(data))
        .catch(error => res.status(400).send({ text: error }));
}

export const api = functions.https.onRequest(app);