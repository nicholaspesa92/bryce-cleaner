"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const helpers = require("./helpers");
const config_1 = require("./config");
// CHARGES //
config_1.app.post('/charges', (req, res) => {
    const userId = req.user.uid;
    const sourceId = req.body.sourceId;
    const amount = req.body.amount;
    const currency = req.body.currency;
    const promise = helpers.createCharge(userId, sourceId, amount, currency);
    defaultHandler(promise, res);
});
config_1.app.get('/charges', (req, res) => {
    const userId = req.user.uid;
    const promise = helpers.getUserCharges(userId);
    defaultHandler(promise, res);
});
// POST Sources //
config_1.app.post('/sources', (req, res) => {
    console.log(req.user);
    const userId = req.user.uid;
    const sourceId = req.body.sourceId;
    const promise = helpers.attachSource(userId, sourceId);
    defaultHandler(promise, res);
});
// GET Customer //
config_1.app.get('/customer', (req, res) => {
    const userId = req.user.uid;
    const promise = helpers.getCustomer(userId);
    defaultHandler(promise, res);
});
config_1.app.post('/customer', (req, res) => {
    const userId = req.user.uid;
    const cusData = req.body.cusData;
    const promise = helpers.updateCustomer(userId, cusData);
    defaultHandler(promise, res);
});
// POST Subscriptions //
config_1.app.post('/subscriptions', (req, res) => {
    const userId = req.user.uid;
    const sourceId = req.body.sourceId;
    const planData = req.body.planData;
    const metadata = req.body.metadata;
    const promise = helpers.createSubscription(userId, sourceId, planData, metadata);
    defaultHandler(promise, res);
});
config_1.app.get('/plans', (req, res) => {
    const userId = req.user.uid;
    const promise = helpers.getPlans(userId);
    defaultHandler(promise, res);
});
config_1.app.post('/email', (req, res) => {
    const userId = req.user.uid;
    const data = req.body.data;
    const promise = helpers.sendConfirmationEmail(userId, data);
    defaultHandler(promise, res);
});
function defaultHandler(promise, res) {
    promise
        .then(data => res.status(200).send(data))
        .catch(error => res.status(400).send({ text: error }));
}
exports.api = functions.https.onRequest(config_1.app);
//# sourceMappingURL=api.js.map